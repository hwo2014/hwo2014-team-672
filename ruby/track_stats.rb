class TrackStats
  # attr_accessor @trackInfo, @corners

  @corners

  @trackInfo

  def initialize
    @corners = []
  end

  def trackInfo= trackInfo
    @trackInfo = trackInfo
  end

  def trackInfo
    return @trackInfo
  end

  def corners
    return @corners
  end


  # Pieces have a length (if straight)
  # Pieces have an angle and radius (if corner)
  # Pieces can have a switch
  # def calculate_shortest_route trackInfo

  #   trackInfo['lanes'].each do |lane|
  #     pointer = 0
  #     while pointer < trackInfo['pieces'].length
  #       section = calculate_section_length trackInfo['pieces'], pointer, lane['distanceFromCenter']
  #       pointer = section[:switchIndex] + 1
  #       puts "Length for lane #{lane['index']} in section #{pointer} = #{section[:length]}"
  #     end
  #   end
  # end

  # # returns length for every piece and lane for a section, starting from pieceIndex and ending when a switch is hit
  # # {:length => length of lane, :switchIndex => the end of the section}
  # def calculate_section_length pieces, pieceIndex, laneOffset
  #   piece = pieces[pieceIndex]
    
  #   laneLength = 0
  #   lastIndex = 0
  #   # check if piece is a switch or the last piece of the track
  #   if (piece['switch'].nil? || !piece['switch']) && !pieces[pieceIndex+1].nil?
  #       # if it's not, recurse this function and concat any output to sectionLengths
  #       lengthCalc = calculate_section_length pieces, (pieceIndex+1), laneOffset
  #       laneLength = lengthCalc[:length]
  #       lastIndex = lengthCalc[:switchIndex]
  #   else
  #       lastIndex = pieceIndex
  #   end

  #   if !piece['length'].nil?
  #     # straight piece
  #     laneLength += piece['length']
  #   elsif !piece['radius'].nil? && !piece['angle'].nil?
  #     # corner piece, lets calculate lane radius. Positive angle = right. Positive lane offset = right
  #     laneRadius = piece['angle'] >= 0 ? 
  #                  piece['radius'] - laneOffset : 
  #                  piece['radius'] + laneOffset
  #     # calculate part of circumverence
  #     laneLength += (piece['angle'].abs / RAD) * (laneRadius *  Math::PI)
  #   end

  #   return {:length => laneLength, :switchIndex => lastIndex}
  # end

  RAD = 180.to_f

  # a corner is a subsequent set of pieces with the same radius and pos/neg angle
  def calculate_corners
    # (l)eft (r)ight or (s)traight
    i = 0
    while i < @trackInfo['pieces'].length do
      piece = @trackInfo['pieces'][i]
      if piece['angle'].nil? && piece['radius'].nil?
        i += 1
        next
      end
      dir = piece['angle'] >= 0 ? 'r' : 'l'
      cornerResult = { 'startIndex' => i, 'endIndex' => i, 'angle' => 0, 'lanes' => [] }
      @trackInfo['lanes'].each do |lane|
        cornerLaneResult = calculate_corner_for_lane i, dir, lane
        cornerResult['endIndex'] = cornerLaneResult['endIndex']
        clritem = {'id' => lane['index'], 
                   'length' => cornerLaneResult['length'], 
                   'factor' => (piece['angle']/cornerLaneResult['length']).abs}
        cornerResult['lanes'].push clritem
      end
      pieceLength = (cornerResult['endIndex']-cornerResult['startIndex'])
      cornerResult['angle'] = piece['angle'] * pieceLength
      i += pieceLength
      @corners.push cornerResult
    end
  end

  def calculate_corner_for_lane startPiece, direction, lane
    cornerReturn = {}
    piece = @trackInfo['pieces'][startPiece]
    # check if piece is a corner piece and still in the same direction
    if (!piece['angle'].nil? && !piece['radius'].nil?) &&
       ((piece['angle'] >= 0 && direction == 'r') || (piece['angle'] < 0  && direction == 'l'))
      cornerReturn = calculate_corner_for_lane startPiece+1, direction, lane
    else
      # case of the last piece of the corner
      lastIndex = startPiece
      return {'startIndex' => lastIndex, 'endIndex' => lastIndex, 'length' => 0}
    end
    # lets calculate lane radius. Positive angle = right. Positive lane offset = right
    laneRadius = piece['angle'] >= 0 ? piece['radius'] - lane['distanceFromCenter'] : 
                 piece['radius'] + lane['distanceFromCenter']
    # calculate part of circumverence
    laneLength = (piece['angle'].abs / RAD) * (laneRadius *  Math::PI)
    cornerReturn['startIndex'] = startPiece
    cornerReturn['length'] = laneLength
    return cornerReturn
  end
end